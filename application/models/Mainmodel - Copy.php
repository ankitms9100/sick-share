<?php
Class Mainmodel extends CI_Model
{
	function encrypt($id)
	{
		$key = "nvRytErtGF#%$#";
		$id = base_convert($id, 10, 36); // Save some space
		$data = @mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
		$data = bin2hex($data);
		return $data;
	}

	function decrypt($encrypted_id)
	{
		$key = "nvRytErtGF#%$#";
		$data = @pack('H*', $encrypted_id); // Translate back to binary
		$data = @mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
		$data = base_convert($data, 36, 10);
		return $data;
	}
	     
	function register($data)
	{
	 	if(!empty($data)){	 
			$this->db->where('email', $data['email']); 
			$q = $this->db->get('tbl_users');
			$result = $q->result_array();
			if (!empty($result)) {
				return 'already';
			}else{
				$currntdate = mktime(date("H") , date("i") , date("s") , date("m") , date("d") , date("Y"));
				$dataa = array(
					'username' => $data['username'],
					'email' => $data['email'],
					'password' => md5($data['password']),
					'mp_pass' => $data['password'],
					'adddate' => $currntdate
				);
				$this->db->insert('tbl_users', $dataa);
				$insert_id = $this->db->insert_id();
				if ($insert_id) { 
					return $insert_id;
				}
				else {
				return '';
				}
			} 
			return '';
			 
		}else {
			return '';
		}
	} 

	function login($data)
	{
		$this->db->where('password', md5($data['password']));
		$this->db->where('email', $data['email']); 
		$q = $this->db->get('tbl_users');
		$result = $q->result_array();
		if (!empty($result)) {
			return $result;
		}
		else {  
				return ''; 
		}
	}  
	function updateprofile($data)
	{
	 	if(!empty($data)){	 			 
			$dataa = array(
				'username' => $data['username'],
				'email' => $data['email']
			);
			$this->db->where('id', $data['userId']); 
			$this->db->update('tbl_users', $dataa);
			$insert_id = $this->db->insert_id();
			return 'update';			 
		}else {
			return '';
		}
	} 
   
} 

?>