<?php
Class Mainmodel extends CI_Model
{
	function encrypt($id)
	{
		$key = "nvRytErtGF#%$#";
		$id = base_convert($id, 10, 36); // Save some space
		$data = @mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
		$data = bin2hex($data);
		return $data;
	}

	function decrypt($encrypted_id)
	{
		$key = "nvRytErtGF#%$#";
		$data = @pack('H*', $encrypted_id); // Translate back to binary
		$data = @mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
		$data = base_convert($data, 36, 10);
		return $data;
	}

	function register($data)
	{
	 	if(!empty($data)){
		if($data['is_social']==1){
			if(isset($data['mobile']) && $data['mobile']!=''){
				$this->db->where('mobile', $data['mobile']);
				$q = $this->db->get('tbl_users');
				$result = $q->result_array();
				if (!empty($result)) {
					$dataa = array(
					'device_id' => $data['device_id'],
					'latitude' => $data['latitude'],
					'longitude' => $data['longitude']
				);
					$this->db->where('id', $result[0]['id']);
					$this->db->update('tbl_users', $dataa);
					return $result[0]['id'];
				}
			}

			$this->db->where('email', str_replace('"','',$data['email']));
			$q = $this->db->get('tbl_users');
			$result = $q->result_array();
			if (!empty($result)) {
				$dataa = array(
					'device_id' => $data['device_id'],
					'latitude' => $data['latitude'],
					'longitude' => $data['longitude']
				);
					$this->db->where('id', $result[0]['id']);
					$this->db->update('tbl_users', $dataa);
				return $result[0]['id'];
			}else{
				$currntdate = mktime(date("H") , date("i") , date("s") , date("m") , date("d") , date("Y"));

					/* 'dob' => $data['dob'],
					'mobile' => $data['mobile'], */
				$dataa = array(
					'fname' => $data['fname'],
					'lname' => $data['lname'],
					'gender' => $data['gender'],
					'email' => str_replace('"','',$data['email']),
					'device_id' => $data['device_id'],
					'is_social' => $data['is_social'],
					'social_token' => $data['social_token'],
					'latitude' => $data['latitude'],
					'longitude' => $data['longitude'],
					'adddate' => $currntdate,
					"image" =>(isset($data['image']) && ($data['image'] != ""))?$data['image']:"",
					"thumbnail" =>(isset($data['thumbnail']) && ($data['thumbnail'] != ""))?$data['thumbnail']:"",
				);
				$this->db->insert('tbl_users', $dataa);
				$insert_id = $this->db->insert_id();
				if ($insert_id) {
					return $insert_id;
				}
				else {
				return '';
				}
			}
			return '';
		}else{
			if(isset($data['mobile']) && $data['mobile']!=''){
				$this->db->where('mobile', $data['mobile']);
				$q = $this->db->get('tbl_users');
				$result = $q->result_array();
				if (!empty($result)) {
					return 'already';
				}
			}

			$this->db->where('email', str_replace('"','',$data['email']));
			$q = $this->db->get('tbl_users');
			$result = $q->result_array();
			if (!empty($result)) {
				return 'already';
			}else{
				$currntdate = mktime(date("H") , date("i") , date("s") , date("m") , date("d") , date("Y"));
				/* 'dob' => $data['dob'],
					'mobile' => $data['mobile'],
					 */
				$dataa = array(
					'fname' => $data['fname'],
					'lname' => $data['lname'],
					'gender' => $data['gender'],
					'email' => str_replace('"','',$data['email']),
					'password' => md5($data['password']),
					'mp_pass' => $data['password'],
					'device_id' => $data['device_id'],
					'is_social' => $data['is_social'],
					'social_token' => $data['social_token'],
					'latitude' => $data['latitude'],
					'longitude' => $data['longitude'],
					'adddate' => $currntdate
				);
				$this->db->insert('tbl_users', $dataa);
				$insert_id = $this->db->insert_id();
				if ($insert_id) {
					return $insert_id;
				}
				else {
				return '';
				}
			}
			return '';
		}
		}else {
			return '';
		}
	}

	function reportPost($data)
	{
		$dataa = array(
			'user_id' => $data['user_id'],
			'post_id' => $data['post_id'],
		);
		$this->db->insert('tbl_reported', $dataa);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			return $insert_id;
		}
		else {
			return 'zero';
		}
	}

	function blockUser($data)
	{
		$dataa = array(
			'blocked_by' => $data['user_id'],
			'blocked_user' => $data['other_user_id'],
		);
		$this->db->insert('tbl_blocked', $dataa);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			return $insert_id;
		}
		else {
			return '';
		}
	}

	function getAllPost($date,$url){
		$this->db->where('blocked_by',$date['userId']);
		$blocked = $this->db->get('tbl_blocked');
		$blocked = $blocked->result_array();
		$blocked_users = [];
		for($p=0;$p<count($blocked);$p++){
			array_push($blocked_users, $blocked[$p]['blocked_user']);
		}
		$this->db->order_by('id','DESC');
		if (!empty($blocked)) {
			$this->db->where_not_in('user_id', $blocked_users);
		}
		$q = $this->db->get('tbl_post');
		$result = $q->result_array();
		if (!empty($result)) {
			$info= array();
			 for($p=0;$p<count($result);$p++){
				 $userdetail = $this->getuserbyid($result[$p]['user_id']);
				// total of likes
				$this->db->select('count(id) as likecount');
				$this->db->where('post_id',$result[$p]['id']);
				$q = $this->db->get('tbl_post_like');
				$likecountArr = $q->result_array();
				// total of comment
				 $this->db->select('count(id) as commentcount');
				$this->db->where('post_id',$result[$p]['id']);
				$q = $this->db->get('tbl_post_comment');
				$commentcountArr = $q->result_array();
				 $this->db->select('id');
				$this->db->where('user_id',$date['userId']);
				$this->db->where('post_id',$result[$p]['id']);
				$q = $this->db->get('tbl_post_like');
				$ischecklist = $q->result_array();
				if(!empty($ischecklist)){
					$info[$p]['isLiked']=1;
				}else{
					$info[$p]['isLiked']=0;
				}
				 $info[$p]['id']=$result[$p]['id'];
				 $info[$p]['post_title']=$result[$p]['post_title'];
				 if($result[$p]['post_image']!=''){
					$info[$p]['post_image']=$url.'user_image/post_image/'.$result[$p]['post_image'];
				 }else{
					$info[$p]['post_image']='';
				 }
				 $info[$p]['post_text']=$result[$p]['post_content'];
				 $info[$p]['date']=$result[$p]['adddate'];

				 $info[$p]['user_id']=$userdetail[0]['id'];
				 $info[$p]['user_name']=$userdetail[0]['fname'].' '.$userdetail[0]['lname'];
				 $info[$p]['gender']=$userdetail[0]['gender'];
				 if($userdetail[0]['image']!=''){
					$info[$p]['user_image']=$url.'user_image/image/'.$userdetail[0]['image'];
				}else{
					$info[$p]['user_image']='';
				}

				 $info[$p]['like_count']=$likecountArr[0]['likecount'];
				 $info[$p]['comment_count']=$commentcountArr[0]['commentcount'];

				// $info[$p]['user_location']=$result[$p]['post_text'];
			 }
			return $info;
		}else{
			return '';
		}
	}
	function searchPost($date,$url){
		$this->db->or_like('p.post_title',$date['content']);
		$this->db->or_like('p.post_content',$date['content']);
		$this->db->or_like('u.fname',$date['content']);
		$this->db->or_like('u.lname',$date['content']);

		$this->db->join('tbl_users u','u.id=p.user_id');


		$this->db->order_by('p.id','DESC');
		$q = $this->db->get('tbl_post p');
		$result = $q->result_array();
		if (!empty($result)) {
			$info= array();
			 for($p=0;$p<count($result);$p++){
				 $userdetail = $this->getuserbyid($result[$p]['user_id']);
				// total of likes
				$this->db->select('count(id) as likecount');
				$this->db->where('post_id',$result[$p]['id']);
				$q = $this->db->get('tbl_post_like');
				$likecountArr = $q->result_array();
				// total of comment
				 $this->db->select('count(id) as commentcount');
				$this->db->where('post_id',$result[$p]['id']);
				$q = $this->db->get('tbl_post_comment');
				$commentcountArr = $q->result_array();
				 $this->db->select('id');
				$this->db->where('user_id',$date['userId']);
				$this->db->where('post_id',$result[$p]['id']);
				$q = $this->db->get('tbl_post_like');
				$ischecklist = $q->result_array();
				if(!empty($ischecklist)){
					$info[$p]['isLiked']=1;
				}else{
					$info[$p]['isLiked']=0;
				}
				 $info[$p]['id']=$result[$p]['id'];
				 $info[$p]['post_title']=$result[$p]['post_title'];
				 if($result[$p]['post_image']!=''){
					$info[$p]['post_image']=$url.'user_image/post_image/'.$result[$p]['post_image'];
				 }else{
					$info[$p]['post_image']='';
				 }
				 $info[$p]['post_text']=$result[$p]['post_content'];
				 $info[$p]['date']=$result[$p]['adddate'];

				 $info[$p]['user_id']=$userdetail[0]['id'];
				 $info[$p]['user_name']=$userdetail[0]['fname'].' '.$userdetail[0]['lname'];
				 if($userdetail[0]['image']!=''){
					$info[$p]['user_image']=$url.'user_image/image/'.$userdetail[0]['image'];
				}else{
					$info[$p]['user_image']='';
				}

				 $info[$p]['like_count']=$likecountArr[0]['likecount'];
				 $info[$p]['comment_count']=$commentcountArr[0]['commentcount'];

				// $info[$p]['user_location']=$result[$p]['post_text'];
			 }
			return $info;
		}else{
			return '';
		}
	}
	function getAge($dob) {
    return floor((strtotime(date('d-m-Y')) - strtotime($dob))/(60*60*24*365.2421896));
	}
	function login($data)
	{
		if($data['is_social']=='0'){
			$this->db->where('password', md5($data['password']));
			$this->db->where("(email='".$data['username']."' OR mobile='".$data['username']."')", NULL, FALSE);
			$q = $this->db->get('tbl_users');
			$result = $q->result_array();
			if (!empty($result)) {
				$dataa = array(
					'latitude' => $data['latitude'],
					'longitude' => $data['longitude'],
					'device_id' => $data['device_id']
				);

				$this->db->where('id',$result[0]['id']);
				$this->db->update('tbl_users', $dataa);
				return $result;
			}
			else {
					return '';
			}
		}else{
			$this->db->where('email', trim($data['username']));
			$q = $this->db->get('tbl_users');
			$result = $q->result_array();
			if (!empty($result)) {
				$dataa = array(
					'social_token' => ($data['social_token'])?$data['social_token']:null,
					'latitude' => $data['latitude'],
					'longitude' => $data['longitude'],
					'device_id' => $data['device_id']
				);

				$this->db->where('id',$result[0]['id']);
				$this->db->update('tbl_users', $dataa);
				return $result;
			}
			else {
					return '';
			}
		}
	}
	function updateprofile($data)
	{
	 	if(!empty($data)){
			$dataa = array(
				'fname' => $data['fname'],
				'lname' => $data['lname'],
				'email' => $data['email']
			);
			$this->db->where('id', $data['userId']);
			$this->db->update('tbl_users', $dataa);
			//$insert_id = $this->db->insert_id();
			return 'update';
		}else {
			return '';
		}
	}
	
	function updatenotificationsetting($data){
		if(!empty($data)){
			$dataa = array(
				'is_notification' => $data['is_notification']
			);
			$this->db->where('id', $data['userId']);
			$this->db->update('tbl_users', $dataa);
			return 'update';
		}else {
			return '';
		}
	}
	
	function getuserprofilebyid($data,$url)
	{
	 	if(!empty($data)){
			if($data['otheruserId']=='0'){
			$this->db->where('id', trim($data['userId']));
			$q = $this->db->get('tbl_users');
			$result = $q->result_array();
			if (!empty($result)) {
				$user=array();
				$medical_conditions=array();
				$interests=array();
				$this->db->select('medical_condition.id,medical_condition.name');
				$this->db->from('medical_condition');
				$this->db->group_by('medical_condition.id');
				$this->db->where('user_medical_conditions.userid',$data['userId']);
				$this->db->join('user_medical_conditions', 'user_medical_conditions.medical_condition_id =medical_condition.id', 'left');
				$q = $this->db->get();
				$resulta = $q->result_array();
				/* $this->db->where('user_id',$result[0]['id']);
				$q = $this->db->get('medical_condition');
				$this->db->join('user_medical_conditions', 'user_medical_conditions.userid = tbl_users.id', 'left');
				$resulta = $q->result_array(); */
				if (!empty($resulta)) {
					for($r=0;$r<count($resulta);$r++){
						$medical_conditions[$r]['id']=$resulta[$r]['id'];
						$medical_conditions[$r]['name']=$resulta[$r]['name'];
					}
				}
				/* $this->db->where('user_id',$result[0]['id']);
				$q = $this->db->get('interest');
				$resultas = $q->result_array();
				if (!empty($resultas)) {
					for($r=0;$r<count($resultas);$r++){
						$interests[$r]['id']=$resultas[$r]['id'];
						$interests[$r]['name']=$resultas[$r]['name'];
					}
				}
				 */
				$user['username']=$result[0]['username']; 
				$user['fname']=$result[0]['fname'];
				$user['lname']=$result[0]['lname'];
				$user['id']=$result[0]['id'];
				if($result[0]['dob']!=''){
					$user['age']=$this->getAge($result[0]['dob']);
				}else{
					$user['age']='';
				}
				$user['dob']=$result[0]['dob'];
				$user['email']=$result[0]['email'];
				$user['mobile']=$result[0]['mobile'];
				$user['bio']=$result[0]['bio'];
				if($result[0]['image']!=''){
					$user['image_url']=$url.'user_image/image/'.$result[0]['image'];
				}else{
					$user['image_url']='';
				}
				if($result[0]['image']!=''){
					$user['thumbnail_url']=$url.'user_image/image/'.$result[0]['image'];
				}else{
					$user['thumbnail_url']='';
				}
				$user['gender']=$result[0]['gender'];
				$user['distance'] = "";
				$user['status'] =0;
				/* $user['interests']=$interests; */
				$user['medical_conditions']=$medical_conditions;

				return $user;
			}
		}else{
			$userId = $data['userId'];
			$otheruserId = $data['otheruserId'];
			$this->db->where('id', $userId);
			$q = $this->db->get('tbl_users');
			$resultaa = $q->result_array();
			if (!empty($resultaa)) {
				$latitude = $resultaa[0]['latitude'];
				$longitude = $resultaa[0]['longitude'];
			}
			$this->db->where('tbl_users.id', $otheruserId);
			$this->db->select("tbl_users.*,(((acos(sin((".$latitude."*pi()/180)) * sin((tbl_users.latitude *pi()/180))+cos((".$latitude."*pi()/180)) * cos((tbl_users.latitude *pi()/180)) * cos(((".$longitude."- tbl_users.longitude)*pi()/180))))*180/pi())*60*1.1515) as distance");
			$this->db->from('tbl_users');
			$this->db->order_by("tbl_users.fname", "asc");
			$q = $this->db->get();
			$resulta = $q->result_array();
			$dataa=array();
			if (!empty($resulta)) {
			$d=0;
				$medical_conditions=array();
				$this->db->select('medical_condition.id,medical_condition.name');
				$this->db->from('medical_condition');
				$this->db->group_by('medical_condition.id');
				$this->db->where('user_medical_conditions.userid',$otheruserId);
				$this->db->join('user_medical_conditions', 'user_medical_conditions.medical_condition_id =medical_condition.id', 'left');
				$q = $this->db->get();
				$resultam = $q->result_array();
				if (!empty($resultam)) {
					for($r=0;$r<count($resultam);$r++){
						$medical_conditions[$r]['id']=$resultam[$r]['id'];
						$medical_conditions[$r]['name']=$resultam[$r]['name'];
					}
				}
				$dataa[$d]['id'] = $resulta[0]['id'];
				$dataa[$d]['fname'] =$resulta[0]['fname'];
				$dataa[$d]['lname'] =$resulta[0]['lname'];
				$dataa[$d]['gender'] =$resulta[0]['gender'];
				if($resulta[0]['dob']!=''){
					$dataa[$d]['age'] = $this->getAge($resulta[0]['dob']);
				}
				$dataa[$d]['bio'] =$resulta[0]['bio'];
				$dataa[$d]['medical_conditions'] =$medical_conditions;
				if($resulta[0]['image']!=''){
					$dataa[$d]['thumbnail_url']=$url.'user_image/image/'.$resulta[0]['image'];
				}else{
					$dataa[$d]['thumbnail_url']='';
				}
				$dataa[$d]['distance'] = ($resulta[0]['distance'])?$resulta[0]['distance']:"";
				$checkconnection = $this->getconnectionuserbybothid($data['userId'],$otheruserId);

				$dataa[$d]['status'] =0;

				if(!empty($checkconnection)){
					$dataa[$d]['status'] = $checkconnection[0]['status'];
				}

				return $dataa;
			}
		}
		}else {
			return '';
		}
	}

	function get_unread_notifs_count($userId)
	{
		if($userId != ''){
				//list 1 for general notifications
				$this->db->select('id');
				$this->db->where('user_id',$userId);
				$q = $this->db->get('tbl_notification');
				$resulta = $q->result_array();
				$general_count = 0;
				if (!empty($resulta)) {
					$general_count = count($resulta);
				}

				$this->db->select('*');
				$this->db->where('otherUserId',$userId);
				$this->db->where('status',2);
				$this->db->order_by('id','desc');
				$q = $this->db->get('connection');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					$general_count = $general_count + count($resulta);
				}


				$message=array();
				$counting = 0;
				$count_query = $this->db->query("SELECT * FROM `tbl_message` WHERE `sender_id`=".$userId." AND `is_read`=0");
				$num_count = $count_query->num_rows();
				$counting = $counting + $num_count;
				$info=array();
				$info['general'] = $general_count;
				$info['chat'] = $counting;
				return $info;
		}
		$info=array();
		$info['general'] = 0;
		$info['chat'] = 0;
		return $info;
	}

	function getMedicalConditionsById($data)
	{
	 	if(!empty($data)){
				$this->db->where('id',$data['id']);
				$q = $this->db->get('medical_condition');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					return $resulta;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}
	function getNotificationByUserid($data,$url)
	{
	 	if(!empty($data)){
				//list 1 for general notifications
				$this->db->select('id,message,status as type,message_status as status,adddate as date,sender_id');
				$this->db->where('user_id',$data['userId']);
				$this->db->order_by('id','desc');
				$q = $this->db->get('tbl_notification');
				$resulta = $q->result_array();
				$info=array();
				if (!empty($resulta)) {
					for($n=0;$n<count($resulta);$n++){
						$senderData = $this->getuserbyid($resulta[$n]['sender_id']);
						$info[$n]['id']=$resulta[$n]['id'];
						$info[$n]['message']=$resulta[$n]['message'];
						$info[$n]['type']=$resulta[$n]['type'];
						$info[$n]['status']=$resulta[$n]['status'];
						$info[$n]['date']=$resulta[$n]['date'];
						$info[$n]['from_id']=$resulta[$n]['sender_id'];
						$info[$n]['name']=$senderData[0]['fname'].' '.$senderData[0]['lname'];
						if($senderData[0]['image']!=''){
							$info[$n]['image_url']=$url.'user_image/image/'.$senderData[0]['image'];
						}else{
							$info[$n]['image_url']='';
						}
					}
				}
				//list 1 for general notifications


				//list 2 for get pending friend requests
				$this->db->select('*');
				$this->db->where('otherUserId',$data['userId']);
				$this->db->where('status',2);
				$this->db->order_by('id','desc');
				$q = $this->db->get('connection');
				$resulta = $q->result_array();
				$info2=array();
				if (!empty($resulta)) {
					for($n=0;$n<count($resulta);$n++){
						$senderData = $this->getuserbyid($resulta[$n]['user_id']);

						$info2[$n]['id']=0;
						$info2[$n]['type']='Request';
						$info2[$n]['status']='unread';
						$info2[$n]['date']=$resulta[$n]['added_date'];
						$info2[$n]['from_id']=$resulta[$n]['user_id'];
						$info2[$n]['name']=$senderData[0]['fname'].' '.$senderData[0]['lname'];

						$info2[$n]['message']=$info2[$n]['name']." has sent you a friend request";

						if($senderData[0]['image']!=''){
							$info2[$n]['image_url']=$url.'user_image/image/'.$senderData[0]['image'];
						}else{
							$info2[$n]['image_url']='';
						}
					}
				}
				//list 2 for get pending friend requests
				$final_data=array_merge($info2,$info);
				return $final_data;
		}
		return '';
	}


	function changePassword($data)
	{
	 	if(!empty($data)){
				$this->db->where('password',md5($data['old_password']));
				$this->db->where('id',$data['userId']);
				$q = $this->db->get('tbl_users');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					$dataa = array(
						'password' => md5($data['new_password']),
						'mp_pass' => md5($data['new_password'])
					);

					$this->db->where('id',$data['userId']);
					$this->db->update('tbl_users', $dataa);
					return $resulta[0]['id'];
				}else{
					return 'no_match';
				}
		}else{
			return '';
		}
	}
	function general_profileUpdate($data)
	{
	 	if(!empty($data)){

			$this->db->where('id',$data['id']);
			$q = $this->db->update('tbl_users',$data);
			if($q){
				return 1;
			}
			else{
				return 0;
			}

		}else{
			return 0;
		}
	}
	function userimageupload($data,$img,$thumb)
	{
	 	if(!empty($data)){
			$dataa = array(
				'image' => $img,
				'thumbnail' => $thumb
			);

			$this->db->where('id',$data['userId']);
			$this->db->update('tbl_users', $dataa);
			return 'Done';

		}else{
			return '';
		}
	}
	function updateNotificationStatus($data)
	{
	 	if(!empty($data)){
			$notification = explode(",",$data['notif_id']);
			if(count($notification) > 1){
				foreach($notification as $noti_id)
				{
					$this->db->where('id',$noti_id);
					$this->db->delete('tbl_notification');
				}
				return 'Done';
			}else{
				$this->db->where('id',$data['notif_id']);
				$this->db->delete('tbl_notification');
			}
		}else{
			return '';
		}
	}
	function getMedicalConditionsByuserid($data)
	{
	 	if(!empty($data)){
				$this->db->select('medical_condition.id,medical_condition.name');
				$this->db->where('user_medical_conditions.userid',$data['userId']);
				$this->db->join('user_medical_conditions', 'user_medical_conditions.medical_condition_id =medical_condition.id', 'left');
				$q = $this->db->get('medical_condition');
				//echo $this->db->last_query();exit;
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					return $resulta;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}
	function getMedicalConditionsbyname($data)
	{
	 	if(!empty($data)){
				$this->db->like('name', $data['name']);
				$q = $this->db->get('medical_condition');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					return $resulta;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}
	function getInterestsById($data)
	{
	 	if(!empty($data)){
				$this->db->where('id',$data['id']);
				$q = $this->db->get('interest');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					return $resulta;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}
	function getInterestsByuserid($data)
	{
	 	if(!empty($data)){
				$this->db->where('user_id',$data['userId']);
				$q = $this->db->get('interest');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					return $resulta;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}
	function getinterestsbyname($data)
	{
	 	if(!empty($data)){
				$this->db->like('name', $data['name']);
				$q = $this->db->get('interest');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					return $resulta;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}
	function getuserbyid($id)
	{
	 	if(!empty($id)){
				$this->db->where('id', $id);
				$q = $this->db->get('tbl_users');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					return $resulta;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}

	function getUserConnection($data,$url)
	{
	 	if(!empty($data)){
        $this->db->where('status','1');
        $queryID = $data['userId'];
        $this->db->where("(user_id = $queryID OR otheruserId = $queryID)");
				$q = $this->db->get('connection');
				$resultac = $q->result_array($data['userId']);
				$dataa = array();
				if (!empty($resultac)) {
					$userId = $data['userId'];
					$this->db->where('id', $userId);
					$q = $this->db->get('tbl_users');
					$resultaa = $q->result_array();
						if (!empty($resultaa)) {
						$latitude = $resultaa[0]['latitude'];
						$longitude = $resultaa[0]['longitude'];
						}
					$d=0;
					//if(!empty($resultaa[0]['latitude']) && !empty($resultaa[0]['longitude'])){
					for($v=0;$v<count($resultac);$v++){
            $other_user_id = $resultac[$v]['otherUserId'];
            if ($other_user_id == $userId) {
              $other_user_id = $resultac[$v]['user_id'];
            }
						$this->db->where('tbl_users.id', $other_user_id);
						$this->db->select("tbl_users.*,(((acos(sin((".$latitude."*pi()/180)) * sin((tbl_users.latitude *pi()/180))+cos((".$latitude."*pi()/180)) * cos((tbl_users.latitude *pi()/180)) * cos(((".$longitude."- tbl_users.longitude)*pi()/180))))*180/pi())*60*1.1515) as distance");
						$this->db->from('tbl_users');
						$this->db->order_by("tbl_users.fname", "asc");
						$q = $this->db->get();
						$resulta = $q->result_array();
						if (!empty($resulta)) {
							$medical_conditions=array();
							$this->db->select('medical_condition.id,medical_condition.name');
							$this->db->from('medical_condition');
							$this->db->group_by('medical_condition.id');
							$this->db->where('user_medical_conditions.userid',$resulta[0]['id']);
							$this->db->join('user_medical_conditions', 'user_medical_conditions.medical_condition_id =medical_condition.id', 'left');
							$q = $this->db->get();
							$resultam = $q->result_array();
							if (!empty($resultam)) {
								for($r=0;$r<count($resultam);$r++){
									$medical_conditions[$r]['id']=$resultam[$r]['id'];
									$medical_conditions[$r]['name']=$resultam[$r]['name'];
								}
							}
							$dataa[$d]['id'] = $resulta[0]['id'];
							$dataa[$d]['fname'] =$resulta[0]['fname'];
							$dataa[$d]['lname'] =$resulta[0]['lname'];
							$dataa[$d]['gender'] =$resulta[0]['gender'];
              $dataa[$d]['status'] ='1';
							$dataa[$d]['medical_conditions'] =$medical_conditions;
							if($resulta[0]['image']!=''){
								$dataa[$d]['thumbnail_url']=$url.'user_image/image/'.$resulta[0]['image'];
							}else{
								$dataa[$d]['thumbnail_url']='';
							}
							$dataa[$d]['distance'] =$resulta[0]['distance'];
							$checkconnection = $this->getconnectionuserbybothid($data['userId'], $other_user_id);
							// $dataa[$d]['is_family'] =$resulta[0]['is_family'];
							// $dataa[$d]['is_connected'] =$resulta[0]['is_connected'];


						}
						$d++;
					}
					
					return $dataa;
				}else{
					return 'no';
				}
		}else{
			return '';
		}
	}
	function getuserbylocationsorting($data,$url)
	{
	 	if(!empty($data)){
					$userId = $data['userId'];
					$sorting = $data['sorting'];
					$this->db->where('id', $userId);
					$q = $this->db->get('tbl_users');
					$resultaa = $q->result_array();
					if (!empty($resultaa)) {
						$latitude = $resultaa[0]['latitude'];
						$longitude = $resultaa[0]['longitude'];
					}
						$this->db->select("tbl_users.*,(((acos(sin((".$latitude."*pi()/180)) * sin((tbl_users.latitude *pi()/180))+cos((".$latitude."*pi()/180)) * cos((tbl_users.latitude *pi()/180)) * cos(((".$longitude."- tbl_users.longitude)*pi()/180))))*180/pi())*60*1.1515) as distance");
						$this->db->from('tbl_users');
						$this->db->order_by("distance", $sorting);
						$q = $this->db->get();
						$resulta = $q->result_array();
						if (!empty($resulta)) {
						$dataa=array();
							for($a=0;$a<count($resulta);$a++){
								$medical_conditions=array();
								$this->db->select('medical_condition.id,medical_condition.name');
								$this->db->from('medical_condition');
								$this->db->group_by('medical_condition.id');
								$this->db->where('user_medical_conditions.userid',$resulta[$a]['id']);
								$this->db->join('user_medical_conditions', 'user_medical_conditions.medical_condition_id =medical_condition.id', 'left');
								$q = $this->db->get();
								$resultam = $q->result_array();
								if (!empty($resultam)) {
									for($r=0;$r<count($resultam);$r++){
										$medical_conditions[$r]['id']=$resultam[$r]['id'];
										$medical_conditions[$r]['name']=$resultam[$r]['name'];
									}
								}
								$dataa[$a]['id'] = $resulta[$a]['id'];
								$dataa[$a]['fname'] =$resulta[$a]['fname'];
								$dataa[$a]['lname'] =$resulta[$a]['lname'];
								$dataa[$a]['gender'] =$resulta[$a]['gender'];
								$dataa[$a]['medical_conditions'] =$medical_conditions;
								if($resulta[$a]['image']!=''){
									$dataa[$a]['thumbnail_url']=$url.'user_image/image/'.$resulta[$a]['image'];
								}else{
									$dataa[$a]['thumbnail_url']='';
								}
								$dataa[$a]['distance'] =$resulta[$a]['distance'];
								$dataa[$a]['is_family'] =$resulta[$a]['is_family'];
							}
							return $dataa;
						}

		}else{
			return '';
		}
	}
	function UpdateMessages($userId,$messageIds)
	{
		foreach($messageIds as $msg_id)
		{
			$this->db->where("id",$msg_id);
			//$this->db->where("sender_id",$userId);
			$this->db->where("user_id",$userId);
			$updateArr=array("is_read"=>"1");
			$this->db->update("tbl_message",$updateArr);
		}
		return 1;

	}
	function insertMedicalConditions($data)
	{
	 	if(!empty($data)){
			$insert_id = '';
			$userId = $data->userId;
			$medicalNameArr = $data->medical_conditions;
			$this->db->where('userid', $userId);
			$this->db->delete('user_medical_conditions');
			for($c=0;$c<count($medicalNameArr);$c++){
				if($medicalNameArr[$c]->id=='0'){
					$this->db->where('name', $medicalNameArr[$c]->name);
					$q = $this->db->get('medical_condition');
					$resultaa = $q->result_array();
					if(!empty($resultaa)){
						$dataa = array(
						'medical_condition_id' => $resultaa[0]['id'],
						'userid' => $userId
							);

						$this->db->insert('user_medical_conditions', $dataa);
						$insert_id = $this->db->insert_id();
					}else{
						$dataac = array(
						'name' => $medicalNameArr[$c]->name
						);

						$this->db->insert('medical_condition', $dataac);
						$insert_id_C = $this->db->insert_id();
						$dataa = array(
						'medical_condition_id' => $insert_id_C,
						'userid' => $userId
							);

						$this->db->insert('user_medical_conditions', $dataa);
						$insert_id = $this->db->insert_id();
					}
				}else{
					$dataa = array(
						'medical_condition_id' => $medicalNameArr[$c]->id,
						'userid' => $userId
					);

					$this->db->insert('user_medical_conditions', $dataa);
					$insert_id = $this->db->insert_id();
				}
				//print_r($dataa);
			}
			//exit;
				if ($insert_id) {
					return $insert_id;
				}
		}else{
			return '';
		}
	}

	function insertInterest($data)
	{
	 	if(!empty($data)){
			$userId = $data['userId'];
			$medicalNameArr = explode(',',$data['interestName']);
			for($c=0;$c<count($medicalNameArr);$c++){
				$dataa = array(
					'name' => $medicalNameArr[$c],
					'user_id' => $userId
				);
				$this->db->insert('interest', $dataa);
				$insert_id = $this->db->insert_id();
			}
				if ($insert_id) {
					return $insert_id;
				}
		}else{
			return '';
		}
	}
	function userAddConnection($data)
	{
	 	if(!empty($data)){

			$userId = $data['userId'];

			$otherUserId = $data['otherUserId'];
			$where_cond=' (user_id="'.$userId.'" and otherUserId="'.$otherUserId.'" ) or (otherUserId ="'.$userId.'" and user_id="'.$otherUserId.'" ) ';
			$this->db->where($where_cond);

			//$this->db->where('user_id', $userId);
			//$this->db->where('otherUserId',$otherUserId);

			$this->db->order_by('id','DESC');
			$q = $this->db->get('connection');


			$resultaa = $q->result_array();
			if(empty($resultaa)){
			$dataaa = array(
				'user_id' => $userId,
				'otherUserId' => $otherUserId

			);
			$this->db->insert('connection', $dataaa);
			$insert_id = $this->db->insert_id();


			$userdetail = $this->getuserbyid($userId);
			$otheruserdetail = $this->getuserbyid($otherUserId);
			$message=$userdetail[0]['fname'].' '.$userdetail[0]['lname'].' sent you a friend request';

			//$this->insertnotificationdata($otherUserId,$message,'Request',$userId);


			$this->sendnotificationByonesingle($otheruserdetail[0]['device_id'],$message,'Request');


				if ($insert_id) {

					return $insert_id;
				}
		}
		else{
			return '0';
		}
		}else{
			return '';
		}
	}
	function updateConnectionStatus($data){
		if(!empty($data)){

		$where_cond=' (user_id="'.$data['userId'].'" and otherUserId="'.$data['from_id'].'" ) or (otherUserId ="'.$data['userId'].'" and user_id="'.$data['from_id'].'" ) ';
		$this->db->where($where_cond);
		//$this->db->where('user_id', $data['from_id']);
		//$this->db->where('otherUserId', $data['userId']);
		$q = $this->db->get('connection');
		$resultaa = $q->result_array();

		if(!empty($resultaa)){

			$userdetail = $this->getuserbyid($data['userId']);
			$senderuserdetail = $this->getuserbyid($data['from_id']);

			if($data['status']==0)
			{
				$this->db->where($where_cond);
				$this->db->delete("connection");
				$ret= 3;

				$log_type="Decline";

				$message=$userdetail[0]['fname'].' '.$userdetail[0]['lname'].' declined your request';
			}
			elseif($data['status']==1)
			{
				$dataaf = array('status' => 1);
				$this->db->where($where_cond);
				$this->db->update("connection",$dataaf);
				 $log_type="Accept";

				$message=$userdetail[0]['fname'].' '.$userdetail[0]['lname'].' accepted your request';


				$ret= 1;
			}

			$this->insertnotificationdata($data['from_id'],$message,$log_type,$data['userId']);
			$this->sendnotificationByonesingle($senderuserdetail[0]['device_id'],$message,'Accept');


			}else{
				$ret= 0;
			}

			return $ret;
		}
	}
	function getAllChartMessage($data,$url){
		if(!empty($data)){

					//$q = $this->db->query("select * from tbl_message where id in (select max(id) from tbl_message WHERE (sender_id=".$data['userId']." OR user_id = ".$data['userId'].") GROUP by user_id) order by date DESC ");
					$q = $this->db->query("select sender_id as sender,
							  user_id as receiver, message,
							  max(id) as last_id,
							  date as last_timestamp
							from tbl_message
							  where (user_id = ".$data['userId']." or sender_id = ".$data['userId'].")
							group by
							  least(sender_id, user_id),
							  greatest(sender_id, user_id) order by max(last_timestamp) DESC");
					$resultaa = $q->result_array();
					if(!empty($resultaa)){
						$message = array();
						$res = array();
						foreach($resultaa as $userrecords){
							if($data['userId'] == $userrecords['sender']){
								$userdetail = $this->getuserbyid($userrecords['receiver']);
								$message_query = $this->db->query("SELECT * FROM `tbl_message` WHERE `id`=".$userrecords['last_id']."");
								$messagedata = $message_query->row_array();
								$message['message'] = $messagedata['message'];
								$message['date']=$messagedata['date'];
								$message['id']=$userrecords['last_id'];
								$message['sender_id']=$userdetail[0]['id'];
								$message['sender_name']=$userdetail[0]['fname'].' '.$userdetail[0]['lname'];
								if($userdetail[0]['image']!=''){
									$message['thumbnail_url']=$url.'user_image/image/'.$userdetail[0]['image'];
								}else{
									$message['thumbnail_url']='';
								}
								
								//$count_query = $this->db->query("SELECT * FROM `tbl_message` WHERE (`sender_id`=".$userrecords['receiver']." AND user_id=".$data['userId']." AND `is_read`=0)");
								
							}else{
								$userdetail = $this->getuserbyid($userrecords['sender']);
								$message_query = $this->db->query("SELECT * FROM `tbl_message` WHERE `id`=".$userrecords['last_id']."");
								$messagedata = $message_query->row_array();
								$message['message'] = $messagedata['message'];
								$message['date']=$messagedata['date'];
								$message['id']=$userrecords['last_id'];
								$message['sender_id']=$userdetail[0]['id'];
								$message['sender_name']=$userdetail[0]['fname'].' '.$userdetail[0]['lname'];
								if($userdetail[0]['image']!=''){
									$message['thumbnail_url']=$url.'user_image/image/'.$userdetail[0]['image'];
								}else{
									$message['thumbnail_url']='';
								}
								
								
							}
							$count_query = $this->db->query("SELECT * FROM `tbl_message` WHERE user_id ='".$data['userId']."' AND is_read = 0");
							$message['unread_count']=$count_query->num_rows();
							
							$res[] = $message;
							
							
						}
						return $res;
					}else{
						return '';
					}
					die;
					/*$resultaa = $q->result_array();
					print_r($resultaa);
					die('die');
					if(!empty($resultaa)){
					$message=array();
						for($m=0;$m<count($resultaa);$m++){
						    $userdetail = $this->getuserbyid($resultaa[$m]['user_id']);
							
							$msgq = $this->db->query("select * from tbl_message where id in (select max(id) from tbl_message WHERE (user_id=".$data['userId']."  AND  sender_id=".$userdetail[0]['id'].") OR (user_id=".$userdetail[0]['id']."  AND  sender_id=".$data['userId'].") GROUP by user_id) order by date DESC ");
							$msgresult = $msgq->result_array();
							if($msgresult){
								$message[$m]['message'] = $msgresult[0]['message'];
								$message[$m]['date']=$msgresult[0]['date'];
							}else{
								$message[$m]['message'] = $resultaa[$m]['message'];
								$message[$m]['date']=$resultaa[$m]['date'];
							}
							$message[$m]['id']=$resultaa[$m]['id'];
							$message[$m]['sender_id']=$userdetail[0]['id'];
							$message[$m]['sender_name']=$userdetail[0]['fname'].' '.$userdetail[0]['lname'];
							if($userdetail[0]['image']!=''){
								$message[$m]['thumbnail_url']=$url.'user_image/image/'.$userdetail[0]['image'];
							}else{
								$message[$m]['thumbnail_url']='';
							}
							
							$count_query = $this->db->query("SELECT * FROM `tbl_message` WHERE `sender_id`=".$data['userId']." AND `user_id`=".$userdetail[0]['id']." AND `is_read`=0");
							$message[$m]['unread_count']=$count_query->num_rows();
							//$message['temp'] = $temp;
						}
						return $message;
					}else{
						$q = $this->db->query("select * from tbl_message where id in (select max(id) from tbl_message WHERE user_id=".$data['userId']." GROUP by user_id) order by date DESC ");
							$resultaa = $q->result_array();
							$userdetail = $this->getuserbyid($resultaa[0]['sender_id']);
							$msgq = $this->db->query("select * from tbl_message where id in (select max(id) from tbl_message WHERE (user_id=".$data['userId']."  AND  sender_id=".$userdetail[0]['id'].") OR (user_id=".$userdetail[0]['id']."  AND  sender_id=".$data['userId'].") GROUP by user_id) order by date DESC ");
							$msgresult = $msgq->result_array();
							if($msgresult){
								$message[0]['message'] = $msgresult[0]['message'];
							}else{
								$message[0]['message'] = $resultaa[0]['message'];
							}
							$message[0]['id']=$resultaa[0]['id'];
							$message[0]['sender_id']=$userdetail[0]['id'];
							$message[0]['sender_name']=$userdetail[0]['fname'].' '.$userdetail[0]['lname'];
							if($userdetail[0]['image']!=''){
								$message[0]['thumbnail_url']=$url.'user_image/image/'.$userdetail[0]['image'];
							}else{
								$message[0]['thumbnail_url']='';
							}
							$message[0]['date']=$resultaa[0]['date'];
							$count_query = $this->db->query("SELECT * FROM `tbl_message` WHERE `sender_id`=".$data['userId']." AND `user_id`=".$userdetail[0]['id']." AND `is_read`=0");
							$message[0]['unread_count']=$count_query->num_rows();
							return $message;
							//return '';
					}*/
		}
	}
	/* function getAllChartMessage($data,$url){
		if(!empty($data)){

					$this->db->where('user_id', $data['userId']);
					$this->db->select('sender_id');
					$this->db->group_by('sender_id');
					$q = $this->db->get('tbl_message');
					//echo $this->db->last_query();
					$resultaa = $q->result_array();
					if(!empty($resultaa)){
					$message=array();
						for($m=0;$m<count($resultaa);$m++){
						$this->db->where('sender_id', $resultaa[$m]['sender_id']);
						$this->db->where('user_id', $data['userId']);
						$this->db->order_by('date','desc');
						$q = $this->db->get('tbl_message');
						$resultaad = $q->result_array();
						$userdetail = $this->getuserbyid($resultaa[$m]['sender_id']);
							$message[$m]['id']=$resultaad[0]['id'];
							$message[$m]['sender_id']=$resultaa[$m]['sender_id'];
							$message[$m]['sender_name']=$userdetail[0]['fname'].' '.$userdetail[0]['lname'];
							if($userdetail[0]['image']!=''){
								$message[$m]['thumbnail_url']=$url.'user_image/image/'.$userdetail[0]['image'];
							}else{
								$message[$m]['thumbnail_url']='';
							}
							$message[$m]['message']=$resultaad[0]['message'];
							$message[$m]['date']=$resultaad[0]['date'];
						}
						return $message;
					}else{
						return '';
					}
		}
	} */
	function getAllChartMessageBySender($data,$url){
		if(!empty($data)){

					//$this->db->where('user_id', $data['userId']);
					//$this->db->or_where('user_id', $data['senderId']);
					$sql = "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))";
					$this->db->query($sql);
					$this->db->order_by('date', 'ASC');
					$this->db->group_by('date');
					$this->db->where("(`user_id` = ".$data['userId']." and `sender_id` = ".$data['senderId'].") OR (`user_id` = ".$data['senderId']." and `sender_id` = ".$data['userId'].")");
					$q = $this->db->get('tbl_message');
					$resultaa = $q->result_array();
					//echo $this->db->last_query();exit;
					if(!empty($resultaa)){
					$message=array();
						for($m=0;$m<count($resultaa);$m++){
							$userdetail = $this->getuserbyid($resultaa[$m]['user_id']);
							$senderuserdetail = $this->getuserbyid($resultaa[$m]['sender_id']);
							$message[$m]['id']=$resultaa[$m]['id'];
							$message[$m]['sender_id']=$resultaa[$m]['sender_id'];
							$message[$m]['sender_name']=$senderuserdetail[0]['fname'].' '.$senderuserdetail[0]['lname'];
							if($userdetail[0]['image']!=''){
								$message[$m]['thumbnail_url']=$url.'user_image/image/'.$senderuserdetail[0]['image'];
							}else{
								$message[$m]['thumbnail_url']='';
							}
							$message[$m]['message']=$resultaa[$m]['message'];
							$message[$m]['date']=$resultaa[$m]['date'];
							$message[$m]['is_read']=$resultaa[$m]['is_read'];
						}
						return $message;
					}else{
						return '';
					}
		}
	}
	function searchUser($data,$url)
	{
	 	if(!empty($data)){
		$latitude ='';
		$longitude 	='';
		$userId = $data['userId'];
			$this->db->where('id', $userId);
			$q = $this->db->get('tbl_users');
			$resulta = $q->result_array();
				if (!empty($resulta)) {
				$latitude = $resulta[0]['latitude'];
				$longitude = $resulta[0]['longitude'];
				}
				$valueDate = $data['query'];
				$valueDateArr = explode(' ',$valueDate);
				/* foreach($words as $word){
					$sql[] = 'name LIKE \'%'.$word.'%\'';
				} */


				//$this->db->select("tbl_users.*,3959 *2* acos( cos( radians( '".$latitude."' ) ) * cos( radians( tbl_users.latitude ) ) * cos( radians( tbl_users.longitude ) - radians( '".$longitude."' ) ) + sin( radians( '".$latitude."' ) ) * sin( radians( tbl_users.latitude ) ) )  AS distance");
				// kilometers
				//$this->db->select("tbl_users.*,(((acos(sin((".$latitude."*pi()/180)) * sin((tbl_users.latitude *pi()/180))+cos((".$latitude."*pi()/180)) * cos((tbl_users.latitude *pi()/180)) * cos(((".$longitude."- tbl_users.longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance");
				// miles distance
				$this->db->select("tbl_users.*,(((acos(sin((".$latitude."*pi()/180)) * sin((tbl_users.latitude *pi()/180))+cos((".$latitude."*pi()/180)) * cos((tbl_users.latitude *pi()/180)) * cos(((".$longitude."- tbl_users.longitude)*pi()/180))))*180/pi())*60*1.1515) as distance");
				$this->db->from('tbl_users');
				//if($type=='1'){
					
					$this->db->like('medical_condition.name',$valueDate);
					$this->db->join('user_medical_conditions', 'user_medical_conditions.userid = tbl_users.id', 'left');
					$this->db->join('medical_condition', 'medical_condition.id = user_medical_conditions.medical_condition_id', 'left');
					foreach($valueDateArr as $key => $value) {

							$this->db->or_like('tbl_users.fname', $value,'after');
							$this->db->or_like('tbl_users.lname', $value,'after');
	
					}
					/* foreach($valueDateArr as $key => $value) {
						if($key == 0) {
							$this->db->like('tbl_users.lname', $value,'after');
						} else {
							$this->db->or_like('tbl_users.lname', $value,'after');
						}
					} */
					//$this->db->like('tbl_users.fname',array($valueDateArr),'after');
				/* }else{

					$this->db->like('medical_condition.name',$valueDate);
					$this->db->join('user_medical_conditions', 'user_medical_conditions.userid = tbl_users.id', 'left');
					$this->db->join('medical_condition', 'medical_condition.id = user_medical_conditions.medical_condition_id', 'left');
				} */
				/* if($sort=='1'){
					$this->db->order_by("tbl_users.fname", "asc");
				}else{
				} */
					$this->db->order_by("distance", 'asc');
				$this->db->group_by("tbl_users.id");
				$this->db->where_not_in('tbl_users.id', $userId);
				$q = $this->db->get();
				//echo  $this->db->last_query();exit;
				$resulta = $q->result_array();
					if (!empty($resulta)) {
					$dataa = array();
					for($s=0;$s<count($resulta);$s++){

						$medical_conditions=array();
							$this->db->select('medical_condition.id,medical_condition.name');
							$this->db->from('medical_condition');
							$this->db->group_by('medical_condition.id');
							$this->db->where('user_medical_conditions.userid',$resulta[$s]['id']);
							$this->db->join('user_medical_conditions', 'user_medical_conditions.medical_condition_id =medical_condition.id', 'left');
							$q = $this->db->get();
							$resultam = $q->result_array();
							if (!empty($resultam)) {
								for($r=0;$r<count($resultam);$r++){
									$medical_conditions[$r]['id']=$resultam[$r]['id'];
									$medical_conditions[$r]['name']=$resultam[$r]['name'];
								}
							}
						$dataa[$s]['id'] = $resulta[$s]['id'];
						$dataa[$s]['fname'] = $resulta[$s]['fname'];
						$dataa[$s]['lname'] = $resulta[$s]['lname'];
						$dataa[$s]['gender'] = $resulta[$s]['gender'];
						$dataa[$s]['medical_conditions'] = $medical_conditions;
						if($resulta[$s]['image']!=''){
							$dataa[$s]['thumbnail_url']=$url.'user_image/image/'.$resulta[$s]['image'];
						}else{
							$dataa[$s]['thumbnail_url']='';
						}
						$dataa[$s]['distance'] = $resulta[$s]['distance'];
						$checkconnection = $this->getconnectionuserbybothid($userId,$resulta[$s]['id']);
						//print_r($checkconnection);
						$dataa[$s]['status'] = 0;
						if(!empty($checkconnection)){
							$dataa[$s]['status'] = $checkconnection[0]['status'];
						}
					}
						return $dataa;
					}
			/* }else{
			return '';
			}	 */
		}else{
			return '';
		}
	}
	function searchUserbymdeicalcondiation($data,$url)
	{
	 	if(!empty($data)){
			$userId = $data['userId'];
			$this->db->where('id', $userId);
			$q = $this->db->get('tbl_users');
			$resulta = $q->result_array();
				if (!empty($resulta)) {
				$latitude = $resulta[0]['latitude'];
				$longitude = $resulta[0]['longitude'];
				}
				$valueDate = $data['query'];

				$valueDateArr = explode(' ',$valueDate);

				$this->db->select("tbl_users.*,(((acos(sin((".$latitude."*pi()/180)) * sin((tbl_users.latitude *pi()/180))+cos((".$latitude."*pi()/180)) * cos((tbl_users.latitude *pi()/180)) * cos(((".$longitude."- tbl_users.longitude)*pi()/180))))*180/pi())*60*1.1515) as distance");
				$this->db->from('tbl_users');
				foreach($valueDateArr as $key => $value) {
					if($key == 0) {
						$this->db->like('medical_condition.name', $value);
					} else {
						$this->db->or_like('medical_condition.name', $value);
					}
				}
				$this->db->join('user_medical_conditions', 'user_medical_conditions.userid = tbl_users.id', 'left');
				$this->db->join('medical_condition', 'medical_condition.id = user_medical_conditions.medical_condition_id', 'left');
				/* if($sort=='1'){
					$this->db->order_by("tbl_users.fname", "asc");
				}else{
				} */
					$this->db->order_by("distance", 'asc');
				$this->db->group_by("tbl_users.id");
				$this->db->where_not_in('tbl_users.id', $userId);
				$q = $this->db->get();
				//echo  $this->db->last_query();exit;
				$resulta = $q->result_array();
					if (!empty($resulta)) {
					$dataa = array();
					for($s=0;$s<count($resulta);$s++){

						$medical_conditions=array();
							$this->db->select('medical_condition.id,medical_condition.name');
							$this->db->from('medical_condition');
							$this->db->group_by('medical_condition.id');

							$this->db->where('user_medical_conditions.userid',$resulta[$s]['id']);
							$this->db->join('user_medical_conditions', 'user_medical_conditions.medical_condition_id =medical_condition.id', 'left');
							$q = $this->db->get();
							$resultam = $q->result_array();
							if (!empty($resultam)) {
								for($r=0;$r<count($resultam);$r++){
									$medical_conditions[$r]['id']=$resultam[$r]['id'];
									$medical_conditions[$r]['name']=$resultam[$r]['name'];
								}
							}
						$dataa[$s]['id'] = $resulta[$s]['id'];
						$dataa[$s]['fname'] = $resulta[$s]['fname'];
						$dataa[$s]['lname'] = $resulta[$s]['lname'];
						$dataa[$s]['gender'] = $resulta[$s]['gender'];
						$dataa[$s]['medical_conditions'] = $medical_conditions;
						if($resulta[$s]['image']!=''){
							$dataa[$s]['thumbnail_url']=$url.'user_image/image/'.$resulta[$s]['image'];
						}else{
							$dataa[$s]['thumbnail_url']='';
						}
						$dataa[$s]['distance'] = $resulta[$s]['distance'];
						$checkconnection = $this->getconnectionuserbybothid($userId,$resulta[$s]['id']);
						$dataa[$s]['status'] =0;
						if(!empty($checkconnection)){
							$dataa[$s]['status'] = $checkconnection[0]['status'];
						}
					}
						return $dataa;
					}
			/* }else{
			return '';
			}	 */
		}else{
			return '';
		}
	}
	function searchUserbyusermdeicalcondiation($data,$url)
	{
	 	if(!empty($data)){
			$userId = $data['userId'];
			$this->db->where('id', $userId);
			$q = $this->db->get('tbl_users');
			$resulta = $q->result_array();
				if (!empty($resulta)) {
				$this->db->select('medical_condition_id as id');
				$this->db->where('userid', $userId);
				$q = $this->db->get('user_medical_conditions');
				$medicalIdArr = $q->result_array();
				$latitude = $resulta[0]['latitude'];
				$longitude = $resulta[0]['longitude'];
				}
				if (!empty($latitude) && !empty($longitude)) {
					$valueDate = $data['query'];
					$this->db->select("tbl_users.*,(((acos(sin((".$latitude."*pi()/180)) * sin((tbl_users.latitude *pi()/180))+cos((".$latitude."*pi()/180)) * cos((tbl_users.latitude *pi()/180)) * cos(((".$longitude."- tbl_users.longitude)*pi()/180))))*180/pi())*60*1.1515) as distance");
					$this->db->from('tbl_users');
					//print_r($medicalIdArr);
					$bulkmedicalid = '';
					foreach($medicalIdArr as $key => $value) {
						
						//echo $value['id'];//print_r($value);
						if($bulkmedicalid == '') {
							$bulkmedicalid .="medical_condition.id='".$value['id']."'";

						} else {
							$bulkmedicalid .=" or medical_condition.id='".$value['id']."'";
						}
					}
					$this->db->where("(".$bulkmedicalid.")", NULL, FALSE);
					$this->db->join('user_medical_conditions', 'user_medical_conditions.userid = tbl_users.id', 'left');
					$this->db->join('medical_condition', 'medical_condition.id = user_medical_conditions.medical_condition_id', 'left');
					$this->db->order_by("distance", 'asc');
					$this->db->group_by("tbl_users.id");
					$this->db->where_not_in('user_medical_conditions.userid', $userId);
					$q = $this->db->get();
					$resulta = $q->result_array();
				
					if (!empty($resulta)) {
					$dataa = array();
					for($s=0;$s<count($resulta);$s++){

						$medical_conditions=array();
							$this->db->select('medical_condition.id,medical_condition.name');
							$this->db->from('medical_condition');
							$this->db->group_by('medical_condition.id');

							$this->db->where('user_medical_conditions.userid',$resulta[$s]['id']);
							$this->db->join('user_medical_conditions', 'user_medical_conditions.medical_condition_id =medical_condition.id', 'left');
							$q = $this->db->get();
							$resultam = $q->result_array();
							if (!empty($resultam)) {
								for($r=0;$r<count($resultam);$r++){
									$medical_conditions[$r]['id']=$resultam[$r]['id'];
									$medical_conditions[$r]['name']=$resultam[$r]['name'];
								}
							}
						$dataa[$s]['id'] = $resulta[$s]['id'];
						$dataa[$s]['fname'] = $resulta[$s]['fname'];
						$dataa[$s]['lname'] = $resulta[$s]['lname'];
						$dataa[$s]['gender'] = $resulta[$s]['gender'];
						$dataa[$s]['medical_conditions'] = $medical_conditions;
						if($resulta[$s]['image']!=''){
							$dataa[$s]['thumbnail_url']=$url.'user_image/image/'.$resulta[$s]['image'];
						}else{
							$dataa[$s]['thumbnail_url']='';
						}
						$dataa[$s]['distance'] = $resulta[$s]['distance'];
						$checkconnection = $this->getconnectionuserbybothid($userId,$resulta[$s]['id']);

						if(!empty($checkconnection)){
							$dataa[$s]['status'] = $checkconnection[0]['status'];
							if($checkconnection[0]['status']=='Accept'){
								if($checkconnection[0]['type']!=''){
									$dataa[$s]['is_connected'] = 1;
								}
								if($checkconnection[0]['type']=='2'){
									$dataa[$s]['is_family'] = 1;
								}
							}else{
								$dataa[$s]['is_connected'] = 0;
							$dataa[$s]['is_family'] = 0;
							}
						}else{
							$dataa[$s]['status'] = '';
							$dataa[$s]['is_connected'] = 0;
							$dataa[$s]['is_family'] = 0;
						}
					}
						return $dataa;
					}
			}else{
				return '';
			}	 
		}else{
			return '';
		}
	}
	function removeUserConnection($data){
		if(!empty($data)){
		  $this->db->where('user_id', $data['userId']);
		  $this->db->where('otherUserId', $data['otherUserId']);
		  $this->db->delete('connection');
		  $this->db->where('user_id', $data['otherUserId']);
		  $this->db->where('otherUserId', $data['userId']);
		  $this->db->delete('connection');
		  return 'done';
		}else{
			return '';
		}
	  }
   function createPostByUser($data,$postImg)
	{
	 	if(!empty($data)){
				$dataa = array(
					'user_id' => $data['userId'],
					'post_content' => $data['post_text'],
					'post_title' => $data['post_title'] ,
					'post_image' => $postImg
				);
				$this->db->insert('tbl_post', $dataa);
				$insert_id = $this->db->insert_id();
				if ($insert_id) {
					return $insert_id;
				}
				else {
				return '';
				}
		}else {
			return '';
		}
	}
	function updateUserLocation($data)
	{
	 	if(!empty($data)){
				$dataa = array(
					'longitude' => $data['longitude'],
					'latitude' => $data['latitude']
				);
				$this->db->where('id', $data['userId']);
				$this->db->update('tbl_users', $dataa);
				return 'done';
		}else {
			return '';
		}
	}
	function updateUserbio($data)
	{
	 	if(!empty($data)){
				$dataa = array(
					'bio' => $data['bio']
				);
				$this->db->where('id', $data['userId']);
				$this->db->update('tbl_users', $dataa);
				return 'done';
		}else {
			return '';
		}
	}
	function userLikePost($data)
	{
	 	if(!empty($data)){
			$post_id = $data['post_id'];
			$userId = $data['userId'];
			$this->db->where('user_id', $userId);
			$this->db->where('post_id', $post_id);
			$q = $this->db->get('tbl_post_like');
			$resulta = $q->result_array();
				if (empty($resulta)) {
					$dataa = array(
						'user_id' => $data['userId'],
						'post_id' => $data['post_id']
					);
					$this->db->insert('tbl_post_like', $dataa);
					$insert_id = $this->db->insert_id();
					if ($insert_id) {
					$this->db->select('tbl_post.user_id');
					$this->db->from('tbl_post');
					$this->db->where('id',$data['post_id']);
					$q = $this->db->get();
					$resultam = $q->result_array();
					if(!empty($resultam)){
						$user_id=$resultam[0]['user_id'];
						$userdetail = $this->getuserbyid($userId);
						$senderuserdetail = $this->getuserbyid($user_id);
						$message=$userdetail[0]['fname'].' '.$userdetail[0]['lname'].' likes your post';
						$this->insertnotificationdata($user_id,$message,'Like',$userId);
						$this->sendnotificationByonesingle($senderuserdetail[0]['device_id'],$message,'Like');
					}
						return $insert_id;
					}
					else {
					return '';
					}
				}else{
				return 'already';
				}
		}else {
			return '';
		}
	}
	function insertnotificationdata($user_id,$message,$status,$senderid){
		$dataa = array(
					'user_id' => $user_id,
					'message' => $message,
					'status'=>$status,
					'sender_id'=>$senderid
		);
			$this->db->insert('tbl_notification', $dataa);
			$insert_id = $this->db->insert_id();
	}
	function userCommentPost($data)
	{
	 	if(!empty($data)){
					$dataa = array(
						'user_id' => $data['userId'],
						'post_id' => $data['post_id'],
						'comment' => $data['comment']
					);
					$this->db->insert('tbl_post_comment', $dataa);
					$insert_id = $this->db->insert_id();
					$userdetail = $this->getuserbyid($data['userId']);
					$message=$userdetail[0]['fname'].' '.$userdetail[0]['lname'].' commented on your post';
					$this->db->from('tbl_post');
					$this->db->where('id',$data['post_id']);
					$q = $this->db->get();
					$resultam = $q->result_array();
					$senderuserdetail = $this->getuserbyid($resultam[0]['user_id']);

					$this->insertnotificationdata($resultam[0]['user_id'],$message,'Comment',$data['userId']);
					$this->sendnotificationByonesingle($senderuserdetail[0]['device_id'],$message,'Comment');
					if ($insert_id) {
						return $insert_id;
					}
					else {
					return '';
					}
		}else {
			return '';
		}
	}
	function insertMessage($data)
	{
	 	if(!empty($data)){
					$dataa = array(
						'user_id' => $data['userId'],
						'sender_id' => $data['senderId'],
						'message' => $data['message']
					);
					$userdetail = $this->getuserbyid($data['senderId']); /* OTHER USER */
					$userdetail1 = $this->getuserbyid($data['userId']); /* ME */
					$this->db->insert('tbl_message', $dataa);
					$title='Message';
					$date=date("Y-m-d H:i:s");

					$title='Message';
					$static_message=$userdetail[0]['fname'].' '.$userdetail[0]['lname'].' sent you a message';
					$date=date("Y-m-d H:i:s");
					$insert_id = $this->db->insert_id();
					if ($insert_id) {
						$this->db->select('*');
						$this->db->from('tbl_message');
						$this->db->where('id',$insert_id);
						$q = $this->db->get();
						$resultam = $q->result_array();
						$this->chartsendnotificationByonesingle($userdetail1[0]['device_id'],$data['message'],$static_message,$title,$data['senderId'],$data['userId'],$date,$resultam);
						return $resultam;
					}
					else {
					return '';
					}
		}else {
			return '';
		}
	}
	function getconnectionuserbybothid($userid,$friendId)
	{
	 	if(!empty($userid)){

		$where_cond=' (user_id="'.$userid.'" and otherUserId="'.$friendId.'" ) or (otherUserId ="'.$userid.'" and user_id="'.$friendId.'" ) ';
			$this->db->where($where_cond);


				//$this->db->where('user_id', $userid);
				//$this->db->where('otherUserId', $friendId);
				$q = $this->db->get('connection');
				$resulta = $q->result_array();
				if (!empty($resulta)) {
					return $resulta;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}
	function getPostByPostid($post,$url)
	{
	 	if(!empty($post)){
				$this->db->where('id', $post['postId']);
				$q = $this->db->get('tbl_post');
				$result = $q->result_array();
				if (!empty($result)) {
					$info= array();
						 $userdetail = $this->getuserbyid($result[0]['user_id']);
						// total of likes
						$this->db->select('count(id) as likecount');
						$this->db->where('post_id',$result[0]['id']);
						$q = $this->db->get('tbl_post_like');
						$likecountArr = $q->result_array();
						// total of comment
						 $this->db->select('count(id) as commentcount');
						$this->db->where('post_id',$result[0]['id']);
						$q = $this->db->get('tbl_post_comment');
						$commentcountArr = $q->result_array();

						//
						 $this->db->select('id');
						$this->db->where('user_id',$post['userId']);
						$this->db->where('post_id',$result[0]['id']);
						$q = $this->db->get('tbl_post_like');
						$ischecklist = $q->result_array();
						if(!empty($ischecklist)){
							$info[0]['isLiked']=1;
						}else{
							$info[0]['isLiked']=0;
						}

						// list of comment
						$urlfull = $url.'user_image/image';
						 $this->db->select("pc.id as comment_id,pc.comment,pc.commentdate,concat(u.fname,' ',u.lname) as user_name,concat('".$urlfull."','/',u.image) as user_image,u.id as user_id");
						$this->db->where('pc.post_id',$post['postId']);
						$this->db->join('tbl_users as u','u.id=pc.user_id','left');
						$q = $this->db->get('tbl_post_comment as pc');
						$commentcountArrDetail = $q->result_array();
						$commentcountDetail = array();
						if(!empty($commentcountArrDetail)){
							for($d=0;$d<count($commentcountArrDetail);$d++){
								$commentcountDetail[$d] =$commentcountArrDetail[$d];
							}
						}
						//list of like
						$urlfull = $url.'user_image/image';
						 $this->db->select("pl.id as like_id,pl.likedate,concat(u.fname,' ',u.lname) as user_name,concat('".$urlfull."','/',u.image) as user_image,u.id as user_id");
						$this->db->where('pl.post_id',$post['postId']);
						$this->db->join('tbl_users as u','u.id=pl.user_id','left');
						$q = $this->db->get('tbl_post_like as pl');
						$likeArrDetail = $q->result_array();
						$likeDetail = array();
						if(!empty($likeArrDetail)){
							for($l=0;$l<count($likeArrDetail);$l++){
								$likeDetail[$l] =$likeArrDetail[$l];
							}
						}

						 $info[0]['id']=$result[0]['id'];
						 $info[0]['post_title']=$result[0]['post_title'];
						 if($result[0]['post_image']!=''){
							$info[0]['post_image']=$url.'user_image/post_image/'.$result[0]['post_image'];
						 }else{
							$info[0]['post_image']='';
						 }
						 $info[0]['post_text']=$result[0]['post_content'];
						 $info[0]['date']=$result[0]['adddate'];
						 $info[0]['user_name']=$userdetail[0]['fname'].' '.$userdetail[0]['lname'];
						 if($userdetail[0]['image']!=''){
							$info[0]['user_image']=$url.'user_image/image/'.$userdetail[0]['image'];
						}else{
							$info[0]['user_image']='';
						} 
						 $info[0]['user_id'] = $userdetail[0]['id'];
						 $info[0]['like_count']=$likecountArr[0]['likecount'];
						 $info[0]['comment_count']=$commentcountArr[0]['commentcount'];
						 $info[0]['comment_detail']=$commentcountDetail;
						 $info[0]['like_detail']=$likeDetail;
						 

						// $info[$p]['user_location']=$result[$p]['post_text'];

					return $info;
				}else{
					return '';
				}
		}else{
			return '';
		}
	}
	function sendnotificationByonesingle($device_token,$message,$title){
		 $deviceToken = trim($device_token);
		//function sendMessage(){
			// body
			$content = array(
				"en" => $message
				);
				// title
			$heading = array(
				"en" => $title
				);

		   $fields = array(
					'app_id' => "b95c1f83-f454-4704-a994-1f1363bb61a0",
					'include_player_ids' => array("$deviceToken"),
					'data' => array("foo" => "bar"),
					'contents' => $content,
					'ios_badgeCount' => 134,
					'headings' => $heading
				);

				$fields = json_encode($fields);
				//print("\nJSON sent:\n");
				//print($fields);
				//exit;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
														   'Authorization: Basic ODhiZDRlODYtZGJiOC00Yzg0LWEzYTMtNzZmNGZjNmE3ZjUw'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, FALSE);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

				$response = curl_exec($ch);
				curl_close($ch);

			///return $response;
		//}

		//$data = sendMessage();
		//print_r($data);	\
	}
	function chartsendnotificationByonesingle($device_token,$message,$static_message,$title,$sender,$userId,$date,$messageData){

	 $deviceToken = trim($device_token);
		//function sendMessage(){
			// body
			$content = array(
				"en" => $static_message
				);
				// title
			$heading = array(
				"en" => $title
				);
			$info = $this->mainmodel->get_unread_notifs_count($sender);
			$unread_notif = $info['general'] + $info['chat'];
		   $fields = array(
					'app_id' => "b95c1f83-f454-4704-a994-1f1363bb61a0",
					'include_player_ids' => array("$deviceToken"),
					'data' => array("to_user" => $sender,"from_user" => $userId,"message" => $message,"time" => $date, "message_list" =>$messageData),
					'contents' => $content,
					'ios_badgeType' => 'SetTo',
					'ios_badgeCount' => $unread_notif,
					'headings' => $heading
				);

				$fields = json_encode($fields);
				//print("\nJSON sent:\n");
				//print($fields);
				//exit;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
														   'Authorization: Basic ODhiZDRlODYtZGJiOC00Yzg0LWEzYTMtNzZmNGZjNmE3ZjUw'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, FALSE);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

				$response = curl_exec($ch);
				curl_close($ch);

			///return $response;
		//}

		//$data = sendMessage();
		//print_r($data);	\
	}
}

?>
