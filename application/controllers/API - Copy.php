<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function userregister()
	{
		
		if(!(isset($_REQUEST['username'],$_REQUEST['email'],$_REQUEST['password']))){
			echo json_encode(array("status" => "error", "errorCode" => 9, "message" => "Fields are empty", "state"=>"userregister"));
			exit;
		}
		$username = $this->input->get('username'); 
		$email = $this->input->get('email'); 
		$password = $this->input->get('password'); 
		if($username=='' || $email=='' || $password==''){
			echo json_encode(array("status" => "error", "errorCode" => 9, "message" => "Fields are empty", "state"=>"userregister"));
			exit;
		}	
		 
		$info = $this->mainmodel->register($_REQUEST);
		if(empty($info)){
				$message="Register Unsuccessfully";
				echo json_encode(array("status" => "error", "errorCode" => 1, "message" => $message, "state" => "userregister"));
				exit;
		}elseif($info=='already'){
				$message="This user already exists";
				echo json_encode(array("status" => "error", "errorCode" => 1, "message" => $message, "state" => "userregister"));
				exit;
		}else{
			$message="Register successfully";
			$userDetail = array(
				"id" =>$info,
				"username" =>$username,
				"email" =>$email 
				);
				echo json_encode(array("status" => "success", "errorCode" => 0, "message" => $message, "userDetail" => $userDetail, "state" => "userregister"));
				exit;
		} 
	} 

	public function profileupdate()
	{		
		if(!(isset($_REQUEST['username'],$_REQUEST['email'],$_REQUEST['userId']))){
			echo json_encode(array("status" => "error", "errorCode" => 9, "message" => "Fields are empty", "state"=>"profileupdate"));
			exit;
		}
		$username = $this->input->get('username'); 
		$email = $this->input->get('email'); 
		$userId = $this->input->get('userId'); 
		if($username=='' || $email=='' || $userId==''){
			echo json_encode(array("status" => "error", "errorCode" => 9, "message" => "Fields are empty", "state"=>"profileupdate"));
			exit;
		}	
		 
		$info = $this->mainmodel->updateprofile($_REQUEST);
		if(empty($info)){
				$message="Your information not updated successfully";
				echo json_encode(array("status" => "error", "errorCode" => 1, "message" => $message, "state" => "profileupdate"));
				exit;
		}else{
			$message="Your information updated successfully";
			$userDetail = array(
				"id" =>$userId,
				"username" =>$username,
				"email" =>$email 
				);
				echo json_encode(array("status" => "success", "errorCode" => 0, "message" => $message, "userDetail" => $userDetail, "state" => "profileupdate"));
				exit;
		} 
	} 
	public function login()
	{
		
		if(!(isset($_REQUEST['email'],$_REQUEST['password']))){
			echo json_encode(array("status" => "error", "errorCode" => 9, "message" => "Fields are empty", "state"=>"login"));
			exit;
		} 
		$email = $this->input->get('email'); 
		$password = $this->input->get('password'); 
		if($email=='' || $password==''){
			echo json_encode(array("status" => "error", "errorCode" => 9, "message" => "Fields are empty", "state"=>"login"));
			exit;
		}	
		 
		$info = $this->mainmodel->login($_REQUEST);
		if(empty($info)){
				$message="invalid login";
				echo json_encode(array("status" => "error", "errorCode" => 1, "message" => $message, "state" => "login"));
				exit;
		}else{
			$message="Login successfully";
			$userDetail = array(
				"id" =>$info[0]['id'],
				"username" =>$info[0]['username'],
				"email" =>$info[0]['email'],
				);
				echo json_encode(array("status" => "success", "errorCode" => 0, "message" => $message, "userDetail" => $userDetail, "state" => "userregister"));
				exit;
		}
		

	}

}
